import { useEffect, useState } from 'react'
import cookie from 'react-cookies'
import { loginProps, userProps } from '../types/user_type'

// const CMS_TR_PLUS_COOKIE_TOKEN = "cms-job"
const CMS_TR_PLUS_COOKIE = 'cms-token'

export const useAuth = () => {
  const [user, setUser] = useState<userProps>(cookie.load(CMS_TR_PLUS_COOKIE))
  const login = async ({
    username,
    password,
  }: loginProps): Promise<boolean> => {
    try {
      const expires = new Date()
      expires.setDate(Date.now() + 1000 * 60 * 5)
      cookie.save(
        CMS_TR_PLUS_COOKIE,
        { username },
        {
          path: '/',
          expires: expires,
        },
      )
      return true
    } catch (error) {
      console.log('useAuth -> login', error)
      return false
    }
  }

  const logout = async (): Promise<boolean> => {
    try {
      cookie.remove(CMS_TR_PLUS_COOKIE, { path: '/' })
      return true
    } catch (error) {
      console.log('useAuth -> logout', error)
      return false
    }
  }

  // const getDataUser = async (): Promise<userProps | null> => {
  //   const data: userProps = await cookie.load(CMS_TR_PLUS_COOKIE)
  //   if (data) {
  //     setUser(data)
  //     return data
  //   } else {
  //     return null
  //   }
  // }
  // useEffect(() => {
  //   getDataUser()
  //   return () => {}
  // }, [])

  return {
    user,
    login,
    logout,
  }
}
