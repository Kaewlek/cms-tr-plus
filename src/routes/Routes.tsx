import { Route, Redirect, Switch } from 'react-router-dom'
import Home from '../pages/Home'
import Content from '../pages/Content'
import NotFound from '../pages/NotFound'
import User from '../pages/User'

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/home" component={Home} />
      <Route exact path="/user" component={User} />
      <Route exact path="/content" component={Content} />

      <Route exact path="/404" component={NotFound} />
      <Redirect to="/404" />
    </Switch>
  )
}
