import React from 'react'
import {
  createMuiTheme,
  CssBaseline,
  makeStyles,
  ThemeProvider,
} from '@material-ui/core'
import Header from './components/Layout/Header'
import { styles } from './components/Layout/layout.styles'
import Sidebar from './components/Layout/Sidebar'
import { useAuth } from './hooks/useAuth'
import Login from './pages/Login'
import Routes from './routes/Routes'

const useStyles = makeStyles(styles)
const theme = createMuiTheme({
  typography: {
    fontFamily: 'Kanit',
    h6: {
      fontSize: '0.9rem',
    },
  },
})
function App() {
  const classes = useStyles()
  const { user } = useAuth()
  return (
    <>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        {!user ? (
          <Login />
        ) : (
          <div className={classes.root}>
            <Header />
            <div className={classes.container}>
              <Sidebar />
              <main className={classes.main}>
                <Routes />
              </main>
            </div>
          </div>
        )}
      </ThemeProvider>
    </>
  )
}

export default App
