import { Theme } from '@material-ui/core'
import { indigo } from '@material-ui/core/colors'
import { Styles } from '@material-ui/styles'

export const styles: Styles<Theme, {}, string> = (theme: Theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
  },
  grow: {
    flexGrow: 1,
  },
  app_bar: {
    background: theme.palette.success.main,
    display: 'flex',
  },
  container: {
    display: 'flex',
    flex: 1,
  },
  drawer: {
    background: '#fff',
    width: '240px',
    position: 'static',
    padding: theme.spacing(1.5),
    boxShadow:
      'rgba(0, 0, 0, 0.1) 1px 1px 0px inset, rgba(0, 0, 0, 0.07) 0px -1px 0px inset',
  },
  main: {
    flex: 1,
    background: '#f7f5f5',
    padding: theme.spacing(1.5),
  },
  list: {
    width: '100%',
    maxWidth: 240,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    color: indigo[500],
    height: theme.spacing(5),
    paddingLeft: theme.spacing(4),
  },
  button: {
    color: indigo[500],
    height: theme.spacing(5),
  },
  menu_icon: {
    color: theme.palette.grey[600],
    marginRight:theme.spacing(1.5)
  },
})
