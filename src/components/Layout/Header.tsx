import React, { FunctionComponent } from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import PersonIcon from '@material-ui/icons/Person'
import { Button, makeStyles } from '@material-ui/core'
import { styles } from './layout.styles'
import { useAuth } from '../../hooks/useAuth'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import EditIcon from '@material-ui/icons/Edit'
const useStyles = makeStyles(styles)
const Header: FunctionComponent = ({ children, ...props }) => {
  const classes = useStyles()
  const { user, logout } = useAuth()
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)

  const isMenuOpen = Boolean(anchorEl)

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleMenuClose = () => {
    setAnchorEl(null)
  }

  const handleLogout = async () => {
    const isSuccess = await logout()
    if (isSuccess) window.location.reload()
  }
  const menuId = 'primary-search-account-menu'
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>
        <EditIcon className={classes.menu_icon} />
        <Typography variant="h6" color="textSecondary">
          ข้อมูลส่วนตัว
        </Typography>
      </MenuItem>
      <MenuItem onClick={handleLogout}>
        <ExitToAppIcon className={classes.menu_icon} />
        <Typography variant="h6" color="textSecondary">
          ออกจากระบบ
        </Typography>
      </MenuItem>
    </Menu>
  )

  return (
    <>
      <AppBar position="static" className={classes.app_bar}>
        <Toolbar>
          <Typography className={classes.title} variant="h5" noWrap>
            Thairath+ CMS
          </Typography>
          <div className={classes.grow} />
          <div>
            <Button
              startIcon={<PersonIcon />}
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              {user?.username}
            </Button>
          </div>
        </Toolbar>
      </AppBar>
      {renderMenu}
    </>
  )
}

export default Header
