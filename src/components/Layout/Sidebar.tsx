import { Drawer, List, makeStyles } from '@material-ui/core'
import SettingsIcon from '@material-ui/icons/Settings'
import HomeIcon from '@material-ui/icons/Home'
import PeopleIcon from '@material-ui/icons/People'
import React, { FunctionComponent } from 'react'
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks'
import ListItemLink from '../ListItemLink'
import { styles } from './layout.styles'
const useStyles = makeStyles(styles)
const Sidebar: FunctionComponent = () => {
  const classes = useStyles()
  return (
    <>
      <Drawer
        variant="permanent"
        open
        classes={{
          paper: classes.drawer,
        }}
      >
        <List component="nav" className={classes.list}>
          <ListItemLink
            id="home"
            to="/"
            icon={<HomeIcon />}
            className={classes.button}
            label="หน้าแรก"
          />
          <ListItemLink
            id="content"
            to="/content"
            icon={<LibraryBooksIcon />}
            className={classes.button}
            label="เนื้อหา"
          />
          <ListItemLink
            id="settings"
            icon={<SettingsIcon />}
            className={classes.button}
            label="ดูแลระบบ"
          >
            <List component="div" disablePadding>
              <ListItemLink
                id="user_manager"
                to="/user"
                icon={<PeopleIcon />}
                className={classes.nested}
                label="ผู้ใช้ทั้งหมด"
              />
            </List>
          </ListItemLink>
        </List>
      </Drawer>
    </>
  )
}

export default Sidebar
