import React, { useState } from 'react'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import { useHistory } from 'react-router-dom'
import { Collapse } from '@material-ui/core'
import { ExpandLess, ExpandMore } from '@material-ui/icons'

type ListItemLinkProps = {
  id: string
  to?: string
  label: string
  icon: React.ReactNode
  className?: string
  children?: React.ReactNode
  open?: boolean
}
const ListItemLink = (props: ListItemLinkProps): React.ReactElement => {
  const history = useHistory()
  const [open, setOpen] = useState<boolean>(!!props.open)
  const handleClick = () => {
    if (!props.children && props.to) {
      history.push(props.to)
    } else {
      setOpen(!open)
    }
  }
  return (
    <>
      <ListItem button onClick={handleClick} className={props.className}>
        {props.icon && <ListItemIcon>{props.icon}</ListItemIcon>}
        <ListItemText
          primaryTypographyProps={{
            variant: 'h6',
          }}
          primary={props.label}
        />
        {props.children && (open ? <ExpandLess /> : <ExpandMore />)}
      </ListItem>
      {props.children && (
        <Collapse in={open} timeout="auto" unmountOnExit>
          {props.children}
        </Collapse>
      )}
    </>
  )
}
export default ListItemLink
