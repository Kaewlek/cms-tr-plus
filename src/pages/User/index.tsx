import React, { useEffect, useState } from 'react'
import MUIDataTable, { MUIDataTableColumnDef } from 'mui-datatables'
import user_api from '../../api/user_api'
const columns: Array<MUIDataTableColumnDef> = [
  {
    name: 'username',
    label: 'username',
    options: {
      filter: true,
      sort: true,
    },
  },
  {
    name: 'first_name',
    label: 'ชื่อ',
    options: {
      filter: true,
      sort: true,
    },
  },
  {
    name: 'last_name',
    label: 'นามสกุล',
    options: {
      filter: true,
      sort: true,
    },
  },
  {
    name: 'is_active',
    label: 'สถานะ',
    options: {
      filter: true,
      sort: true,
    },
  },
  {
    name: 'last_update',
    label: 'ใช้งานล่าสุด',
    options: {
      filter: true,
      sort: true,
    },
  },
]
export default function User() {
  const [data, setData] = useState([])
  const fetchData = async () => {
    const res = await user_api.getAllUsers()
    console.log('LOG: response ---> ', res)
    setData(res.data.Admin)
  }
  useEffect(() => {
    fetchData()
    return () => {}
  }, [])
  return (
    <>
      <MUIDataTable
        title={'ผู้ใช้งานทั้งหมด'}
        columns={columns}
        data={data}
        options={{
          print: false,
          download: false,
          selectableRows: 'none',
        }}
      />
    </>
  )
}
