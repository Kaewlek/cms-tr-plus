import React, { Fragment } from 'react'
import {
  FormControl,
  Select,
  Card,
  Typography,
  InputBase,
  makeStyles,
  withStyles,
  Grid,
  Divider,
  Box,
  RadioGroup,
  Radio,
  FormControlLabel,
} from '@material-ui/core'

const useStyles = makeStyles({
  root: {
    maxWidth: '450px',
  },
  box: {
    padding: '20px',
  },
  title: {
    fontSize: 14,
  },
  select: {
    width: '100%',
  },
  input: {
    padding: '10px 26px 10px 12px',
  },
  textLabel: {
    marginTop: '7px',
  },
})

export default function Setting() {
  const classes = useStyles()
  return (
    <Fragment>
      <Card className={classes.root}>
        <Box className={classes.box}>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            ตั้งค่า
          </Typography>
          <Typography variant="h5" component="h2">
            หมวดหมู่เนื้อหาข่าว
          </Typography>
          <Grid container spacing={1}>
            <Grid item xs={5} sm={5}>
              <div className={classes.textLabel}>ชนิดของเนื้อหา</div>
            </Grid>
            <Grid item xs={7} sm={7}>
              <FormControl variant="outlined" className={classes.select}>
                <Select native input={<SelectOptionStyle />}>
                  <option aria-label="None" value="" />
                  <option value={10}>ข่าวออนไลน์</option>
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container spacing={1}>
            <Grid item xs={5} sm={5}>
              <div className={classes.textLabel}>ชนิดของเนื้อหา (ย่อย)</div>
            </Grid>
            <Grid item xs={7} sm={7}>
              <FormControl variant="outlined" className={classes.select}>
                <Select native input={<SelectOptionStyle />}>
                  <option aria-label="None" value="" />
                  <option value={10}>ข่าวออนไลน์</option>
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container spacing={1}>
            <Grid item xs={5} sm={5}>
              <div className={classes.textLabel}>หมวดหมู่</div>
            </Grid>
            <Grid item xs={7} sm={7}>
              <FormControl variant="outlined" className={classes.select}>
                <Select native input={<SelectOptionStyle />}>
                  <option aria-label="None" value="" />
                  <option value={10}>ข่าวออนไลน์</option>
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container spacing={1}>
            <Grid item xs={5} sm={5}>
              <div className={classes.textLabel}>Section</div>
            </Grid>
            <Grid item xs={7} sm={7}>
              <FormControl variant="outlined" className={classes.select}>
                <Select native input={<SelectOptionStyle />}>
                  <option aria-label="None" value="" />
                  <option value={10}>ข่าวออนไลน์</option>
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container spacing={1}>
            <Grid item xs={5} sm={5}>
              <div className={classes.textLabel}>Topic</div>
            </Grid>
            <Grid item xs={7} sm={7}>
              <FormControl variant="outlined" className={classes.select}>
                <Select native input={<SelectOptionStyle />}>
                  <option aria-label="None" value="" />
                  <option value={10}>ข่าวออนไลน์</option>
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container spacing={1}>
            <Grid item xs={5} sm={5}>
              <div className={classes.textLabel}>Content Area</div>
            </Grid>
            <Grid item xs={7} sm={7}>
              <FormControl variant="outlined" className={classes.select}>
                <Select native input={<SelectOptionStyle />}>
                  <option aria-label="None" value="" />
                  <option value={10}>ข่าวออนไลน์</option>
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container spacing={1}>
            <Grid item xs={5} sm={5}>
              <div className={classes.textLabel}>Follow of topic</div>
            </Grid>
            <Grid item xs={7} sm={7}>
              <FormControl variant="outlined" className={classes.select}>
                <Select native input={<SelectOptionStyle />}>
                  <option aria-label="None" value="" />
                  <option value={10}>ข่าวออนไลน์</option>
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </Box>
        <Divider />

        <Box className={classes.box}>
          <Typography variant="h5" component="h2">
            เผยแพร่
          </Typography>
          <Grid container spacing={1}>
            <Grid item xs={5} sm={5}>
              <div className={classes.textLabel}>สถานะการแสดง</div>
            </Grid>
            <Grid item xs={7} sm={7}>
              <FormControl variant="outlined" className={classes.select}>
                <Select native input={<SelectOptionStyle />}>
                  <option aria-label="None" value="" />
                  <option value={10}>ข่าวออนไลน์</option>
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container spacing={1}>
            <Grid item xs={5} sm={5}>
              <div className={classes.textLabel}>พิสูจน์อักษรโดย :</div>
            </Grid>
            <Grid item xs={7} sm={7}>
              <div className={classes.textLabel}>aaaaaaaaa aaaaaaaa</div>
            </Grid>
          </Grid>

          <Grid container spacing={1}>
            <Grid item xs={5} sm={5}>
              <div className={classes.textLabel}>พิสูจน์อักษรแล้ว :</div>
            </Grid>
            <Grid item xs={7} sm={7}>
              <div className={classes.textLabel}>Yes</div>
            </Grid>
          </Grid>

          <Grid container spacing={1}>
            <Grid item xs={5} sm={5}>
              <div className={classes.textLabel}>Credit :</div>
            </Grid>
            <Grid item xs={7} sm={7}>
              <FormControl variant="outlined" className={classes.select}>
                <Select native input={<SelectOptionStyle />}>
                  <option aria-label="None" value="" />
                  <option value={10}>ข่าวออนไลน์</option>
                </Select>
              </FormControl>
              <FormControl component="fieldset">
                <RadioGroup
                  row
                  aria-label="position"
                  name="position"
                  defaultValue="top"
                >
                  <FormControlLabel
                    value="end"
                    control={<Radio color="primary" />}
                    label="ข่าวจากฉบับพิมพ์"
                  />
                  <FormControlLabel
                    value="end"
                    control={<Radio color="primary" />}
                    label="ข่าวออนไลน์"
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container spacing={1}>
            <Grid item xs={5} sm={5}>
              <div className={classes.textLabel}>ช่องทางโซเชียล</div>
            </Grid>
            <Grid item xs={7} sm={7}>
              <FormControl variant="outlined" className={classes.select}>
                <Select native input={<SelectOptionStyle />}>
                  <option aria-label="None" value="" />
                  <option value={10}>ข่าวออนไลน์</option>
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </Box>
      </Card>
    </Fragment>
  )
}

const SelectOptionStyle = withStyles((theme) => ({
  input: {
    borderRadius: 4,
    position: 'relative',
    minWidth: '100px',
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '7px 26px 7px 7px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}))(InputBase)
