import React, { Fragment } from 'react'
import { OutlinedInput, TextareaAutosize ,  makeStyles,Typography,withStyles,InputBase} from '@material-ui/core'
const useStyles = makeStyles({
  title: {
    fontSize: 14,
  },
  input: {
    padding: '10px 26px 10px 12px',
  },
  textWarning: {
    fontSize: 12,
  },
})
export default function Main() {
  const classes = useStyles()
  return (
    <Fragment>
       <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
           สร้างข่าว
          </Typography>
     
      <div>
        หัวข้อ
      </div>
       <OutlinedInput margin="dense"></OutlinedInput>
      <div>โปรยข่าว</div>
      <TextareaAutosize></TextareaAutosize>
      <Typography
            className={classes.textWarning}
            color="textSecondary"
          >
          {'**โปรยข่าว - เนื้อหาโดยสรุปของข่าว ที่จะแสดงในหน้ารายการข่าว'}
          </Typography>
      <div>editor</div>
      <div>
        <TextareaAutosize></TextareaAutosize>
      </div>
      **ข่าว-เนื้อข่าว
      
      <div>ข้อมูลเพิ่มเติม</div>
      <div>Tags แนะนำจากทีม SEO วันนี้</div>
      <div>Tags</div>
    </Fragment>
  )
}

const inputStyly = withStyles((theme) => ({
  input: {
    borderRadius: 4,
    position: 'relative',
    minWidth: '100px',
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '7px 26px 7px 7px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}))(InputBase)