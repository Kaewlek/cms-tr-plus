import React, { Fragment } from 'react'
import Main from './main'
import Setting from './setting'
import { Grid } from '@material-ui/core'
export default function Detail() {
  return (
    <Fragment>
      <Grid container  justify="space-between">
        <Grid item >
          <Main />
        </Grid>
        <Grid item >
          <Setting />
        </Grid>
      </Grid>
    </Fragment>
  )
}
