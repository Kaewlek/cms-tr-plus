import React, { Fragment,FunctionComponent } from 'react'
import Detail from './Detail/index'
import { AppBar, Tabs, Tab, Typography } from '@material-ui/core'
import { makeStyles, withStyles } from '@material-ui/core/styles'

const Content  =({}) =>{

  const classes = useStyles()
  // const [value, setValue] = React.useState(0)

  const handleChange = () => {
    
  }
  return (
    <Fragment>
      <div className={classes.root}>
        <div className={classes.demo1}>
          <AntTabs
            onChange={handleChange}
            aria-label="ant example"
          >
            <AntTab label="ข้อมูลข่าว" />
            <AntTab label="การตีพิมพ์" />
            <AntTab label="Storytelling" />
            <AntTab label="Snippets" />
            <AntTab label="ข้อมูลข่าว" />
            <AntTab label="ไฮไลท์อื่นๆ" />
            <AntTab label="ข่าวแนะนำ" />
            <AntTab label="โปรไฟล์" />
            <AntTab label="ประวัติการบันทึก" />
          </AntTabs>
          <Typography className={classes.padding}>
            <Detail />
          </Typography>
        </div>
      </div>
    </Fragment>
  )
}

const AntTabs = withStyles({
  root: {
    borderBottom: '1px solid #e8e8e8',
  },
  indicator: {
    backgroundColor: '#1890ff',
  },
})(Tabs)
const AntTab = withStyles((theme) => ({
  root: {
    textTransform: 'none',
    minWidth: 72,
    fontWeight: theme.typography.fontWeightRegular,
    marginRight: theme.spacing(4),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      color: '#40a9ff',
      opacity: 1,
    },
    '&$selected': {
      color: '#1890ff',
      fontWeight: theme.typography.fontWeightMedium,
    },
    '&:focus': {
      color: '#40a9ff',
    },
  },
  selected: {},
}))((props) => <Tab disableRipple {...props} />)

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    position: 'relative',
  },
  padding: {
    padding: theme.spacing(3),
  },
  demo1: {
    backgroundColor: theme.palette.background.paper,
  },
  demo2: {
    backgroundColor: '#2e1534',
  },
}))
export default Content