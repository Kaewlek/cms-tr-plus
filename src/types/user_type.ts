export type loginProps = {
  username: string
  password: string
}

export type userProps = {
  username: string
}
