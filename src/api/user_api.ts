import { loginProps, userProps } from '../types/user_type'
import api from './api'
import { gql, QueryResult } from '@apollo/client'
export default {
  login: async (body: loginProps): Promise<userProps> => {
    const data: userProps = await api.thairathPlus('POST', '/', body)
    return data
  },
  logout: async (): Promise<boolean> => {
    return true
  },
  getAllUsers: async () => {
    const FEED_QUERY = gql`
      query FeedQuery {
        Admin {
          _id
          username
          first_name
          last_name
          is_active
          last_update
          roles {
            id
            title
          }
        }
      }
    `
    const data = await api.graphql({
      query: FEED_QUERY,
      // variables:
    })
    return data
  },
}
