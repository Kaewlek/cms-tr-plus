import { ApolloClient, InMemoryCache, QueryOptions } from '@apollo/client'
import Axios, { AxiosResponse, AxiosRequestConfig, Method } from 'axios'

export default {
  /**
   * axios wrappers
   */
  _errorMsg: '',
  thairathPlus: async function (
    method: Method,
    path: string,
    body?: any,
  ): Promise<any> {
    const options: AxiosRequestConfig = {
      method,
      data: body,
      url: `${process.env.REACT_APP_TR_PLUS_API}/${path}`,
      withCredentials: true,
    }
    const response: AxiosResponse = await Axios(options)
    if (response.data.status === 200) {
      return response.data
    } else {
      // console.log(`User lib error ${path}: ${response.data.message}`)
      this._errorMsg = response.data.message
      return response.data
    }
  },
  graphql: async function (options: QueryOptions) {
    const client = new ApolloClient({
      uri: process.env.REACT_APP_GRAPH_QL,
      cache: new InMemoryCache(),
    })
    const res = await client.query(options)
    return res
  },
}
